extends Button

export var position_default = Vector2() setget set_position_default
var position_offset = Vector2() setget set_position_offset
var ignore_focus_sound = false

func set_position_default(value):
	position_default = value
	rect_position = position_default + position_offset

func set_position_offset(value):
	position_offset = value
	rect_position = position_default + position_offset

func _ready():
	position_default = rect_position
	$AnimationFloat.play("animation_float")
	$AnimationFloat.seek(rand_range(0.0, 4.0))
	connect("focus_entered", self, "_on_focus_entered")
	connect("pressed", self, "_on_pressed")

func _on_focus_entered():
	# avoid playing at same time when focused
	if ignore_focus_sound:
		ignore_focus_sound = false
		return
	
	var path = "/root/Main/Focus"
	if has_node(path):
		get_node(path).play()

func _on_pressed():
	var path = "/root/Main/Click"
	if has_node(path):
		get_node(path).play()
