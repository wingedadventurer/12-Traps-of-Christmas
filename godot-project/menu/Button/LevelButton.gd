extends "res://menu/Button/Button.gd"

signal level_focused(number)

export var locked = false setget set_locked

func _ready():
	connect("focus_entered", self, "_on_focus_entered_2")

func set_locked(value):
	locked = value
	if value:
		modulate = Color(0.2, 0.2, 0.2)
	else:
		modulate = Color(1, 1, 1)

func _on_focus_entered_2():
	emit_signal("level_focused", int(text))
