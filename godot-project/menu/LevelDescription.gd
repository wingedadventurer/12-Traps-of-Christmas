extends Label

var level_texts = {
	1: "1 Giant Gift!\n\nEvade the giant gift\nbefore it squashes you!",
	2: "2 Deadly Potions!\n\nCure your poisonuous state!\nFirst drink antidote A,\nthen antidote B!\nHurry!",
	3: "3 Long Needles!\n\nBalance your life on the needles\nand reach the top before you drown!\nYou can DOUBLE-JUMP too!",
	4: "4 Killer Bunnies!\n\nKill them by SHOOTing.\nKill them before they eat you!",
	5: "5 Golden Rings!\n\nJump over them so you\ndon't get rolled over!",
	6: "6 Handy Clouds!\n\nLeap the clouds and reach the top!",
	7: "7 Turtle Backs!\n\nRide them turtles...\n...and don't drown again.\nDASH can be useful.",
	8: "8 Quick Invaders!\n\nThey're coming to abduct you,\nbetter shoot them down fast!\nYou can also SLIDE!",
	9: "9 Tasty Pies!\n\nEat all the red pies,\nbut avoid moldy ones!\n\nLittle wisdom: moldy pies are green...",
	10: "10 Poison Spikes!\n\nSimply avoid the spikes!\n",
	11: "11 Walking Shrooms!\n\nSTOMP them to get extra boost!\nAnd like always, don't drown.",
	12: "12 Godot Parts!\n\nDestroy the evil robot, part by part.\nSTOMP to make the Tree shoot!\nBut you get 3 lives.\nIt's my Christmas gift to you!"
}

func _ready():
	for lvlbtn in get_parent().get_children():
		if lvlbtn.is_in_group("level_button"):
			lvlbtn.connect("level_focused", self, "_on_button_focused")
	visible = false

func _on_button_focused(level_number):
	text = level_texts[level_number]
	visible = true

func _on_back_focused():
	visible = false
