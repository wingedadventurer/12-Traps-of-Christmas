extends Node2D

func _ready():
	for letter in get_children():
		if letter.is_in_group("letter"):
			var anim = letter.get_node("AnimationFloat")
			anim.play("float")
			anim.seek(rand_range(0.0, 4.0))
