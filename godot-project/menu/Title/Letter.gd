extends Sprite

export var float_offset = Vector2() setget set_float_offset
var default_position = Vector2()

func set_float_offset(value):
	float_offset = value
#	position = default_position + float_offset

func _ready():
	default_position = position

func _process(delta):
	position = default_position + float_offset