tool
extends Node2D

export (String, "Red", "Green", "Blue", "Gold") var type = "Red" setget set_type
var locked = true setget set_locked



func _ready():
	set_locked(true)

func _on_Area_input_event(viewport, event, shape_idx):
	if event is InputEventMouseButton and \
	event.button_index == BUTTON_LEFT and \
	event.is_pressed():
		open()

func open():
	$Animation.play("open")
	$Area/Collision.disabled = true
	$Open.play()

func set_locked(value):
	locked = value
	
	if not has_node("Area"): return
	if value == true:
		modulate = Color(0, 0, 0)
		$Area/Collision.disabled = true
	else:
		modulate = Color(1, 1, 1)
		$Area/Collision.disabled = false

func set_type(value):
	if not has_node("Box") or not has_node("Cap"): return
	
	type = value
	
	if value == "Red":
		$Box.frame = 0
		$Cap.frame = 0
	elif value == "Green":
		$Box.frame = 1
		$Cap.frame = 1
	elif value == "Blue":
		$Box.frame = 2
		$Cap.frame = 2
	elif value == "Gold":
		$Box.frame = 3
		$Cap.frame = 3
