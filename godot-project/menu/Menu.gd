extends Node2D

signal level_select(level_number)

func _ready():
	hide_rooms()
	$Main/Play.ignore_focus_sound = true
	$Main/Play.grab_focus()
	
	get_camera().jump_to($Main.position)
	
	# (un)locking levels
	for i in range(11):
		get_node("Levels/L" + str(i + 2)).locked = not Data.progress["l" + str(i + 1)]
	
	# setting settings
	$Settings/MusicToggle.set_state(Data.settings["music"])
	$Settings/SFXToggle.set_state(Data.settings["effects"])
	$Settings/GlowToggle.set_state(Data.settings["glow"])
	$Settings/FullscreenToggle.set_state(Data.settings["fullscreen"])
	$Settings/SnowToggle.set_state(Data.settings["snow"])

func _on_level_select(button):
	emit_signal("level_select", int(button.text))

func get_camera():
	return get_node("/root/Main/Camera")

func hide_rooms():
	for room in get_children():
		room.visible = false
	$Main.visible = true

# buttons

func _on_Play_pressed():
	hide_rooms()
	$Levels.visible = true
	get_camera().reposition($Levels.position)
	$Levels/L1.ignore_focus_sound = true
	$Levels/L1.grab_focus()

func _on_LevelsBack_pressed():
	get_camera().reposition($Main.position)
	$Main/Play.ignore_focus_sound = true
	$Main/Play.grab_focus()

func _on_Tutorials_pressed():
	hide_rooms()
	$Tutorials.visible = true
	get_camera().reposition($Tutorials.position)
	$Tutorials/TutorialsBack.ignore_focus_sound = true
	$Tutorials/TutorialsBack.grab_focus()

func _on_TutorialsBack_pressed():
	get_camera().reposition($Main.position)
	$Main/Tutorials.ignore_focus_sound = true
	$Main/Tutorials.grab_focus()

func _on_Gifts_pressed():
	hide_rooms()
	$Gifts.visible = true
	get_camera().reposition($Gifts.position)
	$Gifts/GiftsBack.ignore_focus_sound = true
	$Gifts/GiftsBack.grab_focus()

func _on_GiftsBack_pressed():
	get_camera().reposition($Main.position)
	$Main/Gifts.ignore_focus_sound = true
	$Main/Gifts.grab_focus()

func _on_Settings_pressed():
	hide_rooms()
	$Settings.visible = true
	get_camera().reposition($Settings.position)
	$Settings/Music.ignore_focus_sound = true
	$Settings/Music.grab_focus()

func _on_SettingsBack_pressed():
	get_camera().reposition($Main.position)
	$Main/Settings.ignore_focus_sound = true
	$Main/Settings.grab_focus()

func _on_Credits_pressed():
	hide_rooms()
	$Credits.visible = true
	get_camera().reposition($Credits.position)
	$Credits/CreditsBack.ignore_focus_sound = true
	$Credits/CreditsBack.grab_focus()

func _on_CreditsBack_pressed():
	get_camera().reposition($Main.position)
	$Main/Credits.ignore_focus_sound = true
	$Main/Credits.grab_focus()

func _on_Quit_pressed():
	get_tree().quit()

# settings

func _on_Music_pressed():
	Data.toggle_setting("music")
	$Settings/MusicToggle.toggle_state()

func _on_SFX_pressed():
	Data.toggle_setting("effects")
	$Settings/SFXToggle.toggle_state()

func _on_Glow_pressed():
	Data.toggle_setting("glow")
	$Settings/GlowToggle.toggle_state()

func _on_Fullscreen_pressed():
	Data.toggle_setting("fullscreen")
	$Settings/FullscreenToggle.toggle_state()

func _on_Snow_pressed():
	Data.toggle_setting("snow")
	$Settings/SnowToggle.toggle_state()

# levels

func _on_L1_pressed():
	if not $Levels/L1.locked:
		get_node("/root/Main")._on_level_select(1)

func _on_L2_pressed():
	if not $Levels/L2.locked:
		get_node("/root/Main")._on_level_select(2)

func _on_L3_pressed():
	if not $Levels/L3.locked:
		get_node("/root/Main")._on_level_select(3)

func _on_L4_pressed():
	if not $Levels/L4.locked:
		get_node("/root/Main")._on_level_select(4)

func _on_L5_pressed():
	if not $Levels/L5.locked:
		get_node("/root/Main")._on_level_select(5)

func _on_L6_pressed():
	if not $Levels/L6.locked:
		get_node("/root/Main")._on_level_select(6)

func _on_L7_pressed():
	if not $Levels/L7.locked:
		get_node("/root/Main")._on_level_select(7)

func _on_L8_pressed():
	if not $Levels/L8.locked:
		get_node("/root/Main")._on_level_select(8)

func _on_L9_pressed():
	if not $Levels/L9.locked:
		get_node("/root/Main")._on_level_select(9)

func _on_L10_pressed():
	if not $Levels/L10.locked:
		get_node("/root/Main")._on_level_select(10)

func _on_L11_pressed():
	if not $Levels/L11.locked:
		get_node("/root/Main")._on_level_select(11)

func _on_L12_pressed():
	if not $Levels/L12.locked:
		get_node("/root/Main")._on_level_select(12)
