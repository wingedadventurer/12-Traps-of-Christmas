extends Sprite

var modulate_active = Color(1.15, 0.7, 0)
var modulate_inactive = Color(1.0, 1.0, 1.0)

func toggle_state():
	if frame == 0: set_state(1) #frame = 1
	else: set_state(0) #frame = 0
	
	if name == "ToggleMusic": Data.settings["music"] = frame
	if name == "ToggleEffects": Data.settings["effects"] = frame
	if name == "ToggleFullscreen": Data.settings["fullscreen"] = frame
	if name == "ToggleSnow": Data.settings["snow"] = frame
	Data.apply_settings()
	Data.save_settings()

func set_state(value):
	frame = value
	$Cross.visible = value
