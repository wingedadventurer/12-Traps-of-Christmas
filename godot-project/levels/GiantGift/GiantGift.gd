extends "res://level/Level.gd"
class_name GiantGift

var scene_gift = preload("res://levels/GiantGift/Gift.tscn")

func init():
	# player
	create_player()
	var r = randi() % 3
	player.position = $PlayerSpawns.get_children()[r].position
	
	# gift
	var gift = scene_gift.instance()
	add_child(gift)
	
	# gift "random" position
	if randi() % 5 == 0: # 20% chance to not be above player
		if r == 0 or r == 2: # if player at sides
			r = 1 # then gift at middle
		elif r == 1: # if player at middle
			r = (randi() % 2) * 2 # then gift at any side
	# else, it will be above player (same "r")
	
	gift.position = $BlockSpawns.get_children()[r].position
	gift.connect("on_ground", self, "_on_Block_on_ground")

func _on_Block_on_ground():
	if not player.dead:
		end()
