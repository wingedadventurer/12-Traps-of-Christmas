extends Sprite

var DELAY_TIME = 0.2
var REVEAL_TIME = 0.4

func _ready():
	$Delay.start(DELAY_TIME)
	modulate.a = 0.0

func _on_Delay_timeout():
	$Tween.interpolate_property(self, "modulate:a", 0.0, 0.3, REVEAL_TIME, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	$Tween.start()
