extends Area2D

signal on_ground

var FALLING_SPEED = 20.0
var GROUND_HEIGHT = 768.0 - 32.0 - 160.0

var velocity = Vector2()
var falling = true

func _ready():
	velocity.y = FALLING_SPEED

func _physics_process(delta):
	if falling:
		position += velocity
		if global_position.y >= GROUND_HEIGHT - 32.0:
			global_position.y = GROUND_HEIGHT
			$Collision.disabled = true
			falling = false
			$Fall.play()
			emit_signal("on_ground")
			get_node("/root/Main/Camera").shake()

func _on_Block_body_entered(body):
	if body.is_in_group("player"):
		body.die()
