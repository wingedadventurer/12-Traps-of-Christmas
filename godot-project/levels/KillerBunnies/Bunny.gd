extends KinematicBody2D

var scene_death = preload("res://levels/KillerBunnies/BunnyDeath.tscn")

var HEALTH_DEFAULT = 1
var GRAVITY_STRENGTH = 3000.0
var HORIZONTAL_SPEED = 500.0

var JUMP_STRENGTH_MIN = 1100.0
var JUMP_STRENGTH_MAX = 1800.0

var JUMP_DELAY_MIN = 0.1
var JUMP_DELAY_MAX = 0.6

var health
var velocity = Vector2()
var is_jumping = false

func _ready():
	health = HEALTH_DEFAULT
	$JumpDelay.start(rand_range(JUMP_DELAY_MIN, JUMP_DELAY_MAX))
	velocity.x = HORIZONTAL_SPEED
	if randi() % 2:
		flip()

func _process(delta):
	if is_jumping:
		velocity.y += GRAVITY_STRENGTH * delta
		
		# horizontal movement
		var movement_h = Vector2(velocity.x, 0)
		if test_move(transform, movement_h * delta, false):
			flip()
			movement_h.x *= -1
		position += movement_h * delta
		
		# vertical movement
		var movement = Vector2(0, velocity.y)
		if test_move(transform, movement * delta, false):
			move_and_collide(movement * delta)
			is_jumping = false
			$JumpDelay.start(rand_range(JUMP_DELAY_MIN, JUMP_DELAY_MAX))
		else:
			position += movement * delta

func flip():
	$Sprite.scale.x *= -1
	velocity.x *= -1

func damage(amount = 1):
	health -= amount
	if health <= 0:
		die()

func jump():
	velocity.y = -rand_range(JUMP_STRENGTH_MIN, JUMP_STRENGTH_MAX)
	is_jumping = true
	$Jump.play()

func die():
	get_parent().kill_a_bunny_you_heartless_monster()
	
	var death = scene_death.instance()
	death.position = position
	get_parent().add_child(death)
	
	queue_free()

func _on_Area_body_entered(body):
	if body.is_in_group("player"):
		body.die()

func _on_Area_area_entered(area):
	if area.is_in_group("bullet"):
		area.queue_free()
		damage()

func _on_JumpDelay_timeout():
	jump()
