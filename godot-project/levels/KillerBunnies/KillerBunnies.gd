extends "res://level/Level.gd"

var scene_bunny = preload("res://levels/KillerBunnies/Bunny.tscn")

var remaining_bunnies

func kill_a_bunny_you_heartless_monster(amount = 1):
	remaining_bunnies -= amount
	if remaining_bunnies <= 0:
		end()

func init():
	# player
	create_player()
	player.position = $SpawnPlayer.position
	
	# bunnies
	for spawn in $BunnySpawns.get_children():
		var bunny = scene_bunny.instance()
		add_child(bunny)
		bunny.position = spawn.position
	remaining_bunnies = 4
