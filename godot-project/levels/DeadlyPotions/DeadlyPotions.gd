extends "res://level/Level.gd"

var scene_potion = preload("res://levels/DeadlyPotions/Potion.tscn")
var required_types = []
var next_required_type_index

var INIT_LIFETIME = 2.0
var POTION_LIFETIME = 1.0
var m_r = 0

func _process(delta):
	if not $Lifetime.is_stopped():
		var m = $Lifetime.time_left
		if m_r:
			if next_required_type_index == 0: player.modulate = Color(m / 2.0, 0, 0)
			else: player.modulate = Color(0, m / INIT_LIFETIME, 0)
		else:
			if next_required_type_index == 1: player.modulate = Color(m / 2.0, 0, 0)
			else: player.modulate = Color(0, m / INIT_LIFETIME, 0)

func init():
	m_r = randi() % 2
	
	# player
	create_player()
	player.position = $PlayerSpawn.position
	player.get_node("Bubbles").emitting = true
	
	# creating potions
	var types
	var potion_a = scene_potion.instance()
	add_child(potion_a)
	potion_a.connect("pickup", self, "_on_potion_pickup")
	var potion_b = scene_potion.instance()
	add_child(potion_b)
	potion_b.connect("pickup", self, "_on_potion_pickup")
	
	# typing potions
	required_types = []
	types = ["AG", "AR"]
	if randi() % 2:
		potion_a.set_type("AG")
		potion_b.set_type("BR")
	else:
		potion_a.set_type("AR")
		potion_b.set_type("BG")
	required_types.append(potion_a.type)
	required_types.append(potion_b.type)
	
	# placing potions
	if randi() % 2:
		potion_a.position = $PotionSpawns.get_children()[0].position
		potion_b.position = $PotionSpawns.get_children()[1].position
	else:
		potion_a.position = $PotionSpawns.get_children()[1].position
		potion_b.position = $PotionSpawns.get_children()[0].position
	
	# other stuff
	next_required_type_index = 0
	$Lifetime.start(INIT_LIFETIME)
	$Drink.pitch_scale = 0.8

func _on_potion_pickup(type):
	if type == required_types[next_required_type_index]:
		next_required_type_index += 1
		$Drink.pitch_scale += 0.2
		$Drink.play()
		$Lifetime.start($Lifetime.time_left + POTION_LIFETIME)
		if next_required_type_index == required_types.size():
			$Lifetime.stop()
			player.modulate = Color(1, 1, 1)
			end()
	else:
		$Lifetime.stop()
		player.die()

func _on_Lifetime_timeout():
	player.die()
