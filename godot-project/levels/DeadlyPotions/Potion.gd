tool
extends Node2D

signal pickup(type)

export (String, "0", "AG", "AR", "BG", "BR") var type = "0" setget set_type

var Frames = {
	"0": 0,
	"AG": 1,
	"AR": 2,
	"BG": 3,
	"BR": 4
}

func set_type(value):
	if not has_node("Sprite"): return
	
	type = value
	$Sprite.frame = Frames[value]

func _on_Area_body_entered(body):
	if body.is_in_group("player"):
		emit_signal("pickup", type)
		set_type("0")
		$Area/Collision.disabled = true
