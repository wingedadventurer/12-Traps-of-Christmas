extends Area2D

signal last

var clockwise = true

var ROTATE_SPEED = 120.0
var HORIZONTAL_VELOCITY = 400.0

var velocity = Vector2()
var last = false

func _ready():
	var r = randi() % 2
	if r == 1:
		HORIZONTAL_VELOCITY *= 1.3
		ROTATE_SPEED *= 1.3
	
	if not clockwise:
		HORIZONTAL_VELOCITY *= -1
		ROTATE_SPEED *= -1
	
	velocity.x = HORIZONTAL_VELOCITY

func _physics_process(delta):
	position += velocity * delta
	rotation_degrees += ROTATE_SPEED * delta
	
	if global_position.x <= -128.0 or global_position.x >= 1152.0:
		if last: emit_signal("last")
		queue_free()

func _on_Ring_body_entered(body):
	if body.is_in_group("player"):
		body.die()
