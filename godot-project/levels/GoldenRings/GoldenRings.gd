extends "res://level/Level.gd"

var scene_ring = preload("res://levels/GoldenRings/Ring.tscn")

var RING_SPAWN_DELAY_MIN = 0.8
var RING_SPAWN_DELAY_MAX = 1.5
var NUMBER_OF_RINGS = 5

var remaining_rings

func init():
	create_player()
	player.position = $PlayerSpawn.position
	
	remaining_rings = NUMBER_OF_RINGS
	
	$RingSpawnDelay.start(START_DELAY)

func restart():
	.restart()
	$RingSpawnDelay.stop()

func spawn_ring():
	var r = randi() % $RingSpawns.get_child_count()
	var spawn = $RingSpawns.get_children()[r]
	
	var ring = scene_ring.instance()
	if r % 2 == 1:
		ring.clockwise = false
	spawn.add_child(ring)
	
	remaining_rings -= 1
	if remaining_rings == 0:
		ring.connect("last", self, "_on_last_ring")
		ring.last = true

func _on_RingSpawnDelay_timeout():
	if remaining_rings > 0:
		spawn_ring()
		$RingSpawnDelay.start(rand_range(RING_SPAWN_DELAY_MIN, RING_SPAWN_DELAY_MAX))

func _on_last_ring():
	if not player.dead:
		end()
