extends RigidBody2D

var scene_bomb_explosion = preload("res://levels/GodotParts/Bomb/BombExplosion.tscn")

var LIFETIME = 3.0
var SPREAD = 0.0

func _ready():
	$Timer.start(LIFETIME)
	$Animation.play("explosion_sign")

func _on_Timer_timeout():
	explode()

func explode():
	for body in $Explosion.get_overlapping_bodies():
		if body.is_in_group("player"):
			body.die()
	var bomb_explosion = scene_bomb_explosion.instance()
	get_parent().add_child(bomb_explosion)
	bomb_explosion.global_position = global_position
	visible = false
	$Collision.disabled = true
	$Explosion/Collision.disabled = true
	queue_free()

func _on_Explosion_area_entered(area):
	if area.is_in_group("bullet"):
		area.queue_free()
		explode()
