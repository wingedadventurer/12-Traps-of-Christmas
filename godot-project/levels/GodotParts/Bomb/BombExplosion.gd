extends Particles2D

func _ready():
	emitting = true
	$Timer.start()
	$Boom.play()

func _on_Timer_timeout():
	queue_free()
