extends "res://level/Level.gd"
class_name GodotParts

var scene_godot = preload("res://levels/GodotParts/Godot.tscn")

func init():
	# spawning player
	create_player()
	player.position = $PlayerSpawn.position
	player.damage = true
	player.connect("ground_hit", self, "_on_player_ground_hit")
	
	# spawning Godot
	var godot = scene_godot.instance()
	add_child(godot)
	godot.position = $GodotSpawn.position

func _on_StompCheck_body_entered(body):
#	if body.is_in_group("player") and body.move_state == body.MoveState.STOMPING:
#		$Tree.shoot()
	pass

func end():
	get_node("/root/Main").data["level 12"] = true
	get_node("/root/Main").save_data()
	$EndDelay.start(3.0)
	$Overlay.fade_out(3.0)

func _on_player_ground_hit():
	$Tree.shoot()
