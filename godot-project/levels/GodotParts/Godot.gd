extends Node2D

var scene_bomb = preload("res://levels/GodotParts/Bomb/Bomb.tscn")
var scene_rocket = preload("res://levels/GodotParts/Rocket/Rocket.tscn")
var scene_laser = preload("res://levels/GodotParts/Laser/Laser.tscn")
var scene_bomb_explosion = preload("res://levels/GodotParts/Bomb/BombExplosion.tscn")
var scene_shard = preload("res://levels/GodotParts/Shard/Shard.tscn")
var scene_shard_eye = preload("res://levels/GodotParts/Shard/ShardEye.tscn")

var ATTACK_DELAY = [0, 1.0, 2.0, 0.5, 0.2, 0.5, 0.0]
var END_DELAY = 3.0

onready var Parts = {
	"DrillLeft": $Skull/ArmLeftPos/ArmLeft/DrillLeftPos,
	"DrillRight": $Skull/ArmRightPos/ArmRight/DrillRightPos,
	"ArmLeft": $Skull/ArmLeftPos,
	"ArmRight": $Skull/ArmRightPos,
	"Spike1": $Skull/Spike2Left,
	"Spike2": $Skull/Spike1Left,
	"Spike3": $Skull/Spike1Right,
	"Spike4": $Skull/Spike2Right,
	"EyeLeft": $Skull/EyeLeft,
	"EyeRight": $Skull/EyeRight,
	"Jaw": $Skull/JawPos/Jaw,
	"Skull": $Skull
}

var PartFrames = {
	"DrillLeft": 3,
	"DrillRight": 3,
	"ArmLeft": 3,
	"ArmRight": 3,
	"Spike1": 2,
	"Spike2": 2,
	"Spike3": 2,
	"Spike4": 2,
	"EyeLeft": 3,
	"EyeRight": 3,
	"Jaw": 5,
	"Skull": 5
}

var PartShardAmount = {
	"DrillLeft": 8,
	"DrillRight": 8,
	"ArmLeft": 12,
	"ArmRight": 12,
	"Spike1": 4,
	"Spike2": 4,
	"Spike3": 4,
	"Spike4": 4,
	"EyeLeft": 3,
	"EyeRight": 3,
	"Jaw": 16,
	"Skull": 40
}

var PartHealths = {
	"DrillLeft": 3,
	"DrillRight": 3,
	"ArmLeft": 1,#3,
	"ArmRight": 1,#3,
	"Spike1": 1,#2,
	"Spike2": 1,#2,
	"Spike3": 1,#2,
	"Spike4": 1,#2,
	"EyeLeft": 3,
	"EyeRight": 3,
	"Jaw": 1,#5,
	"Skull": 1#5
}

var state = 0
var move_type = "circle_small"
var attackdict = {}

# STATES
#
# 1 swings his drills downwards. You are getting used to the fight.
# 2 shoots bombs towards you. You gotta dash/slide away quickly.
# 3 shoots rockets that follow you. (4 total on screen). You can shoot them.
# 4 drops his jaw onto you. Dash/slide away.
# 5 shoots lasers downwards from his eyes.
# 6 smashes your Christmas tree and drops down even harder than the jaw.
#   But he stays in the ground for a while.
#   Stomp on him several times to crack the skull.
#

func _ready():
	$Skull.visible = false
#	change_state(0)
	$Visibility.play("appear")

func _physics_process(delta):
	
	if state == 1: # DRILLS
		# move state
		if Parts["DrillLeft"] == null and \
		   Parts["DrillRight"] == null:
			change_state(2)
		
		check_treebeam("DrillLeft", "DrillLeftArea", "DrillLeft")
		check_treebeam("DrillRight", "DrillRightArea", "DrillRight")
		attack()
	
	elif state == 2: # CANNONS
		# move state
		if Parts["ArmLeft"] == null and \
		   Parts["ArmRight"] == null:
			change_state(3)
		
		# check treebeam
		check_treebeam("ArmLeft", "ArmLeft/ArmLeftArea", "ArmLeft")
		check_treebeam("ArmRight", "ArmRight/ArmRightArea", "ArmRight")
		attack()
	
	elif state == 3: # SPIKES
		# move state
		if Parts["Spike1"] == null and \
		   Parts["Spike2"] == null and \
		   Parts["Spike3"] == null and \
		   Parts["Spike4"] == null:
			change_state(4)
		
		# check treebeam
		check_treebeam("Spike1", "Spike1Area")
		check_treebeam("Spike2", "Spike2Area")
		check_treebeam("Spike3", "Spike3Area")
		check_treebeam("Spike4", "Spike4Area")
		attack()
	
	elif state == 4: # EYES
		# move state
		if Parts["EyeLeft"] == null and \
		   Parts["EyeRight"] == null:
			change_state(5)
		
		# check treebeam
		check_treebeam("EyeLeft", "EyeLeftArea")
		check_treebeam("EyeRight", "EyeRightArea")
		attack()
	
	elif state == 5: # JAW
		# move state
		if Parts["Jaw"] == null:
			change_state(6)
		
		# check treebeam
		check_treebeam("Jaw", "JawArea")
		attack()
	
	elif state == 6: # SKULL
		# move state (endgame)
		if Parts["Skull"] == null:
			get_node("/root/Main/BossMusic").stop()
			get_parent().get_node("Tree").active = false
			$EndDelay.start(END_DELAY)
			state = 7
		
		# check treebeam
		check_treebeam("Skull", "SkullArea")

func change_state(new_state, move_to_spawn = true):
	if new_state == 1:
		attackdict = {
		Parts["DrillLeft"]: "left_arm_swing",
		Parts["DrillRight"]: "right_arm_swing"
		}
		move_type = "circle_small"
	elif new_state == 2:
		attackdict = {
			Parts["ArmLeft"]: "cannons",
			Parts["ArmRight"]: "cannons"
			
		}
		move_type = "circle_medium"
	elif new_state == 3:
		attackdict = {
			Parts["Spike1"]: "spike1",
			Parts["Spike2"]: "spike2",
			Parts["Spike3"]: "spike3",
			Parts["Spike4"]: "spike4"
		}
		move_type = "circle_medium"
	elif new_state == 4:
		attackdict = {
			Parts["EyeLeft"]: "eyes",
			Parts["EyeRight"]: "eyes"
		}
		move_type = "circle_big"
	elif new_state == 5:
		attackdict = {
			Parts["Jaw"]: "jaw"
		}
		move_type = "circle_small"
	elif new_state == 6:
		move_type = "spin"
	
	state = new_state
	$AttackDelayTimer.start(ATTACK_DELAY[state])
	if move_to_spawn: $Movement.play("to_start")
	else: $Movement.play(move_type)

func check_treebeam(part_name, area_path, sprite_path = null):
	var part = Parts[part_name]
	if part == null: return
	var areas = part.get_node(area_path).get_overlapping_areas()
	for area in areas:
		if area.is_in_group("treebeam") and area.get_parent().shooting:
			PartHealths[part_name] -= 1
			if PartHealths[part_name] <= 0:
				# creating explosion
				var bomb_explosion = scene_bomb_explosion.instance()
				add_child(bomb_explosion)
				bomb_explosion.global_position = part.global_position
				$Explosion.play()
				
				# creating shards
				create_shards(part_name, part.global_position)
				
				# removing part
				attackdict.erase(part)
				part.visible = false
				part.get_node(area_path + "/Collision").disabled = true
				Parts[part_name] = null
			else:
				create_shards(part_name, part.global_position)
				$Hit.play()
			
			var s
			if sprite_path == null: s = part
			else: s = part.get_node(sprite_path)
			if PartHealths[part_name] != 0:
				s.frame = PartFrames[part_name] - PartHealths[part_name]
			area.get_parent().shooting = false

func attack():
	if attackdict.empty(): return
	
	if $AttackDelayTimer.is_stopped() and not $Attacks.is_playing():
		
		var r = randi() % attackdict.size()
		if attackdict.keys()[r] != null:
			if not attackdict.values()[r] in ["eyes", "cannons", "spike1", "spike2", "spike3", "spike4"]:
				if $Movement.is_playing(): $Movement.stop()
			var v = attackdict.values()[r]
			if v == "jaw":
				var addons = ["_drop_left", "_drop_right", "_left", "_right"]
				if (randi() % 4 == 0):
					v += addons[randi() % 2 + 2]
				else:
					v += addons[randi() % 2]
			$Attacks.play(v)

func create_bomb(target):
	if not has_node(target): return
	var t = get_node(target)
	if not t.visible: return
	
	var SPREAD = 10.0
	var VELOCITY_MIN = 1000.0
	var VELOCITY_MAX = 1500.0
	
	var bomb = scene_bomb.instance()
	bomb.global_position = t.global_position
	get_parent().add_child(bomb)
	var r = t.rotation + deg2rad(rand_range(-SPREAD * 0.5, SPREAD * 0.5))
	if t.position.x < 0: r += deg2rad(180.0) # quick fix :(
	
	bomb.apply_central_impulse(Vector2(rand_range(VELOCITY_MIN, VELOCITY_MAX), 0).rotated(r))
	$CannonShot.play()

func create_rocket(target):
	if not has_node(target): return
	var t = get_node(target)
	var t2 = get_parent().get_node("Player")
	
	var ROCKET_SPEED = 350.0
	
	var rocket = scene_rocket.instance()
	rocket.global_position = t.global_position
	get_parent().add_child(rocket)
	var r = t.rotation
	if t.position.x < 0: r += deg2rad(180.0) # quick fix :(
	
	rocket.velocity = Vector2(ROCKET_SPEED, 0).rotated(r)
	rocket.target = t2
	$CannonShot.play() # REPLACE WITH ROCKET LAUNCH SOUND!

func create_laser(target):
	if not has_node(target): return
	var t = get_node(target)
	if not t.visible: return
	
	var LASER_SPEED = 1000.0
	var SPREAD = 75.0
	
	var laser = scene_laser.instance()
	t.add_child(laser)

func create_shards(part_name, p = Vector2(0, 0)):
	for i in range(PartShardAmount[part_name]):
		var shard
		if part_name in ["EyeLeft", "EyeRight"]:
			shard = scene_shard_eye.instance()
			shard.modulate = $Skull/EyeLeft.modulate
		else:
			shard = scene_shard.instance()
		shard.global_position = p
		shard.z_index = 20
		get_parent().add_child(shard)

func _on_Attacks_animation_finished(anim_name):
	$AttackDelayTimer.start(ATTACK_DELAY[state])
	if anim_name in ["eyes", "cannons", "left_arm_swing", "right_arm_swing", "spike1", "spike2", "spike3", "spike4"]:
		if not $Movement.is_playing():
			$Movement.play(move_type)
		return
	$Movement.play("to_start")

func _on_body_enter(body):
	if body.is_in_group("player"):
		body.die()

func _on_Visibility_animation_finished(anim_name):
	change_state(1, false)

func _on_Movement_animation_finished(anim_name):
	if anim_name == "to_start":
		$Movement.play(move_type)

func _on_EndDelay_timeout():
	get_parent().end()
