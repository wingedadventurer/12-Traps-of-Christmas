extends RigidBody2D

var SPEED_MIN = 10.0
var SPEED_MAX = 400.0

func _ready():
	$Sprite.frame = randi() % $Sprite.hframes * $Sprite.vframes
	$Sprite.rotation_degrees = rand_range(0.0, 360.0)
	var impulse = Vector2(rand_range(SPEED_MIN, SPEED_MAX), 0).rotated(deg2rad(rand_range(0.0, 360.0)))
	apply_central_impulse(impulse)


func _on_Timer_timeout():
	queue_free()