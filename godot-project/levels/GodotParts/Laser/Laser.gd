extends Area2D

func _ready():
	$Animation.play("beam")

func _on_Laser_body_entered(body):
	if body.is_in_group("player"):
		body.die()

func _on_Animation_animation_finished(anim_name):
	queue_free()
