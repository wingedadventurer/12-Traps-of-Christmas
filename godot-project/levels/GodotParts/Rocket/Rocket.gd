extends Area2D

var scene_bomb_explosion = preload("res://levels/GodotParts/Bomb/BombExplosion.tscn")

var target = null

var SPEED = 100.0
var ROTATE_SPEED = 120.0
var ROTATE_DELAY = 0.1
var velocity = Vector2()
var can_rotate = false
var LIFETIME = 20.0

func _ready():
	velocity = Vector2(SPEED, 0)
	$RotateDelay.start(ROTATE_DELAY)

func _process(delta):
	# rotation
	if target != null and can_rotate and not target.dead:
		var target_position = to_local(target.global_position)
		if velocity.angle_to(target_position) > 0:
			velocity = velocity.rotated(deg2rad(ROTATE_SPEED * delta))
		else:
			velocity = velocity.rotated(deg2rad(-ROTATE_SPEED * delta))
	$Thruster.rotation = velocity.angle() + deg2rad(180.0)
	$Sprite.rotation = velocity.angle()
	
	# movement
	position += velocity * delta
	
	# checking treebeam
	for area in get_overlapping_areas():
		if area.is_in_group("treebeam") and area.get_parent().shooting:
			explode()

func explode():
	var bomb_explosion = scene_bomb_explosion.instance()
	get_parent().add_child(bomb_explosion)
	bomb_explosion.global_position = global_position
	visible = false
	$Collision.call_deferred("set_disabled", true)
	$Explosion/Collision.call_deferred("set_disabled", true)
#	$Collision.disabled = true
#	$Explosion/Collision.disabled = true
	queue_free()

func _on_Rocket_area_entered(area):
	if area.is_in_group("bullet"):
		area.queue_free()
		explode()

func _on_Rocket_body_entered(body):
	if body.is_in_group("player"):
		if body.move_state == body.MoveState.MOVING:
			body.die()
		explode()
	elif body.is_in_group("walls"):
		explode()

func _on_RotateDelay_timeout():
	can_rotate = true

func _on_VisibilityNotifier2D_screen_exited():
	if target != null and target.dead:
		queue_free()
