extends "res://level/Level.gd"
class_name TurtleBacks

var scene_turtle = preload("res://levels/Turtlebacks/Turtle.tscn")

var TURTLE_SPAWN_DELAY_MIN = 1.4
var TURTLE_SPAWN_DELAY_MAX = 1.6
var TURTLE_SPAWN_DELAY_INIT = 0.8
var NUMBER_OF_TURTLES = 7

var remaining_turtles

func _ready():
	._ready()
	
	# permanent water
	$Water.remove_from_group("disposable")

func restart():
	.restart()
	$TurtleSpawnDelay.stop()

func init():
	# spawning default turtle and player
	var turtle = scene_turtle.instance()
	add_child(turtle)
	create_player()
	var r = randi() % 2
	if r == 0:
		turtle.position = $TurtleSpawns/SpawnDefault1.position
		player.position = $PlayerSpawns/PlayerSpawn1.position
	else:
		turtle.position = $TurtleSpawns/SpawnDefault2.position
		turtle.velocity.x *= -1
		turtle.scale.x *= -1
		player.position = $PlayerSpawns/PlayerSpawn2.position
	
	remaining_turtles = NUMBER_OF_TURTLES - 1
	$TurtleSpawnDelay.start(TURTLE_SPAWN_DELAY_INIT)

func spawn_turtle():
	var turtle = scene_turtle.instance()
	if remaining_turtles == 1:
		turtle.last = true
		turtle.connect("stopped", self, "_on_last_turtle_stopped")
	add_child(turtle)
	
	if randi() % 2:
		turtle.position = $TurtleSpawns/Spawn1.position
	else:
		turtle.position = $TurtleSpawns/Spawn2.position
		turtle.velocity.x *= -1
		turtle.scale.x *= -1
	
	
	
	remaining_turtles -= 1

func _on_TurtleSpawnDelay_timeout():
	if remaining_turtles > 0:
		spawn_turtle()
		$TurtleSpawnDelay.start(rand_range(TURTLE_SPAWN_DELAY_MIN, TURTLE_SPAWN_DELAY_MAX))

func _on_last_turtle_stopped():
	end()
