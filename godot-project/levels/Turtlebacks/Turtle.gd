extends Area2D

signal stopped

var last = false
var stopped = false

var HORIZONTAL_SPEED_MIN = 400.0
var HORIZONTAL_SPEED_MAX = 500.0
var HORIZONTAL_DAMPING = 150.0

var velocity = Vector2()

func _ready():
	velocity.x = rand_range(HORIZONTAL_SPEED_MIN, HORIZONTAL_SPEED_MAX)
	$AnimationPlayer.play("walk")
	
	if last: $Sprite.frame = 1

func _physics_process(delta):
	if stopped: return
	
	position += velocity * delta
	
	if last:
		var damp = HORIZONTAL_DAMPING * delta
		velocity.x -= sign(velocity.x) * damp
		if abs(velocity.x) < damp:
			velocity.x = 0
			stopped = true
			emit_signal("stopped")
	
	$AnimationPlayer.playback_speed = velocity.x / HORIZONTAL_SPEED_MAX

func _on_Turtle_body_entered(body):
	if body.name == "Player":
		body.die()
		$Collision.disabled = true
