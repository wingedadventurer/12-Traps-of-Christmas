extends StaticBody2D

var HORIZONTAL_VELOCITY_MIN = 300.0
var HORIZONTAL_VELOCITY_MAX = 400.0

var last = false
var velocity = Vector2()

func _ready():
	velocity.x = rand_range(HORIZONTAL_VELOCITY_MIN, HORIZONTAL_VELOCITY_MAX)
	$Movement.play("walk")
	
	if last: $Sprite.frame = 1

func _process(delta):
	position += velocity * delta
