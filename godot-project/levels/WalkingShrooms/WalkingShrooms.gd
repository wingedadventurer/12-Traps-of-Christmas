extends "res://level/Level.gd"
class_name WalkingShrooms

var scene_shroom = preload("res://levels/WalkingShrooms/Shroom.tscn")
var scene_water = preload("res://level/Water.tscn")
var scene_cloud = preload("res://levels/HandyClouds/Cloud.tscn")

var NUMBER_OF_SHROOMS = 11
var SHROOM_SPAWN_DELAY_MIN = 1.0
var SHROOM_SPAWN_DELAY_MAX = 1.2
var SHROOM_SPAWN_DELAY_INIT = 0.3

var remaining_shrooms

func _ready():
	._ready()
	
	# permanent water
	$Water.remove_from_group("disposable")

func restart():
	.restart()
	$ShroomSpawnDelay.stop()

func spawn_shroom():
	var shroom = scene_shroom.instance()
	if remaining_shrooms == 1: shroom.last = true
	add_child(shroom)
	var r = randi() % 2
	shroom.position = $ShroomSpawns.get_children()[r].position
	if r == 1:
		shroom.velocity.x *= -1
		shroom.get_node("Sprite").scale.x *= -1
	
	remaining_shrooms -= 1

func init():
	# player
	create_player($PlayerSpawn.position)
	
	# cloud
	var cloud = scene_cloud.instance()
	cloud.position = $CloudSpawn.position
	add_child(cloud)
	
	# shrooms
	remaining_shrooms = NUMBER_OF_SHROOMS
	$ShroomSpawnDelay.start(SHROOM_SPAWN_DELAY_INIT)

func _on_ShroomSpawnDelay_timeout():
	if remaining_shrooms > 0:
		spawn_shroom()
		$ShroomSpawnDelay.start(rand_range(SHROOM_SPAWN_DELAY_MIN, SHROOM_SPAWN_DELAY_MAX))
	elif remaining_shrooms == 0:
		remaining_shrooms = -1

func _on_Outside_body_entered(body):
	if body.is_in_group("player"):
#		print("No you won't! :P")
#		body.die()
		end()
#		$Outside/Collision.call_deffered("set_disabled", true)
		
		$Outside.set_deferred("monitoring", false)
		if has_node("Player"):
			$Player.visible = false
