extends "res://level/Level.gd"

var scene_needle = load("res://levels/LongNeedles/Needle.tscn")
var scene_water = load("res://level/Water.tscn")

func init():
	# player
	create_player()
	player.position = $PlayerSpawn.position
	player.position.x += (randi() % 2) * 384
	
	# needles
	var positions = [-96, 0, 96]
	for spawn in $NeedleSpawns.get_children():
		var needle = scene_needle.instance()
		add_child(needle)
		needle.position = spawn.position
		var r = randi() % positions.size()
		needle.position.x += positions[r]
		positions.remove(r)
	
	# water
	var water = scene_water.instance()
	add_child(water)
	water.position = $WaterSpawn.position
	water.rising = true
	water.WATER_HEIGHT_MAX = 420
	water.WATER_RISE_SPEED = 120
	water.connect("water_at_end", self, "_on_water_at_end")

func _on_water_at_end():
	if not player.dead:
		end()
