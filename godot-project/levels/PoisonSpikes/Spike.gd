extends Area2D

signal last

var HORIZONTAL_VELOCITY = 700.0

var velocity = Vector2()
var last = false

func _ready():
	velocity.x = HORIZONTAL_VELOCITY

func _physics_process(delta):
	position += velocity * delta
	
	if global_position.x <= -128.0 or global_position.x >= 1152.0:
		if last: emit_signal("last")
		queue_free()

func _on_Spike_body_entered(body):
	if body.is_in_group("player"):
		body.die()
