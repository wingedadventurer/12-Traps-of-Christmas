extends "res://level/Level.gd"

var scene_spike = preload("res://levels/PoisonSpikes/Spike.tscn")

var SPIKE_SPAWN_TIME = 0.5
var SPIKE_AMOUNT = 10
#var START_DELAY = 0.6

var remaining_spikes
var last_spike_spawner_index = 0

func restart():
	.restart()
	$SpikeSpawnTime.stop()

func init():
	create_player()
	player.position = $PlayerSpawn.position
	
	remaining_spikes = SPIKE_AMOUNT
	$SpikeSpawnTime.start(START_DELAY)

func spawn_spike():
	var spike = scene_spike.instance()
	add_child(spike)
	
	# algorithm to not spawn spike at same level as last one
	var r
	var choices = [0, 1, 2, 3]
	choices.remove(choices.find(last_spike_spawner_index))
#	if last_spike_spawner_index - 2 >= 0:
#		choices.remove(choices.find(last_spike_spawner_index - 2))
#	else:
#		choices.remove(choices.find(last_spike_spawner_index + 2))
	r = choices[randi() % 2]
	
	var spikespawner = $SpikeSpawners.get_children()[r]
	spike.position = spikespawner.position
	spike.velocity = spike.velocity.rotated(spikespawner.rotation)
	spike.rotation = spikespawner.rotation
	last_spike_spawner_index = r
	
	$Shoot.play()
	remaining_spikes -= 1
	if remaining_spikes == 0:
		spike.last = true
		spike.connect("last", self, "_on_last_spike")

func _on_SpikeSpawnTime_timeout():
	if remaining_spikes > 0:
		spawn_spike()
		$SpikeSpawnTime.start(SPIKE_SPAWN_TIME)

func _on_last_spike():
	if not player.dead:
		end()
