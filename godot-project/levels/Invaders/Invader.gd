extends Area2D

var scene_explosion = preload("res://levels/GodotParts/Bomb/BombExplosion.tscn")

signal dead

func _ready():
	$AnimationPlayer.play("tilt")

func die():
	$Collision.disabled = true
	visible = false
	$Death.play()
	emit_signal("dead")
	
	var explosion = scene_explosion.instance()
	explosion.global_position = global_position
	get_parent().get_parent().get_parent().get_parent().add_child(explosion)

func _on_Invader_body_entered(body):
	if body.name == "Player":
		body.die()

func _on_Death_finished():
	queue_free()

func _on_Invader_area_entered(area):
	if area.is_in_group("bullet"):
		area.queue_free()
		die()

func _on_PlayerCheck_body_entered(body):
	if body.is_in_group("player"):
		body.die()
