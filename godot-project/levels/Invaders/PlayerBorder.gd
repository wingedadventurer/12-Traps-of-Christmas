extends Area2D

func _ready():
	$AnimationPlayer.play("float")

func _on_PlayerBorder_body_entered(body):
	if body.is_in_group("player"):
		body.die()
