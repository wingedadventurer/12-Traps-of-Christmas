extends "res://level/Level.gd"

var scene_invader = preload("res://levels/Invaders/Invader.tscn")

var INVADER_SPAWN_DELAY = 1.0
var NUMBER_OF_INVADERS = 8
var INVADER_SPEED = 0.18

var remaining_invaders
var remaining_invaders_for_win

func _process(delta):
	update_paths(delta)

func restart():
	.restart()
	$InvaderSpawnDelay.stop()

func update_paths(delta):
	for path in $Paths.get_children():
		for follow in path.get_children():
			follow.unit_offset += INVADER_SPEED * delta

func spawn_invader():
	var r = randi() % $Paths.get_child_count()
	
	var follow = PathFollow2D.new()
	follow.rotate = false
	follow.loop = false
	var invader = scene_invader.instance()
	follow.add_child(invader)
	$Paths.get_children()[r].add_child(follow)
	invader.connect("dead", self, "_on_invader_dead")
	
	remaining_invaders -= 1

func init():
	create_player()
	player.position = $PlayerSpawn.position
	
	# removing paths
	for path in $Paths.get_children():
		for follow in path.get_children():
			path.remove_child(follow)
			follow.queue_free()
	
	remaining_invaders = NUMBER_OF_INVADERS
	remaining_invaders_for_win = NUMBER_OF_INVADERS
	$InvaderSpawnDelay.start(START_DELAY)

func _on_InvaderSpawnDelay_timeout():
	if remaining_invaders > 0:
		spawn_invader()
		$InvaderSpawnDelay.start(INVADER_SPAWN_DELAY)

func _on_invader_dead():
	remaining_invaders_for_win -= 1
	if remaining_invaders_for_win == 0:
		end()
