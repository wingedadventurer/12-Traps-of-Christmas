extends Area2D

signal last

var FALL_SPEED_MIN = 500.0
var FALL_SPEED_MAX = 700.0
var ROTATION_SPEED_MIN = -90.0
var ROTATION_SPEED_MAX = 90.0

var END_HEIGHT = 880

var bad = false
var velocity = Vector2()
var rotation_speed = 0.0
var last = false

func _ready():
	velocity.y = rand_range(FALL_SPEED_MIN, FALL_SPEED_MAX)
	rotation_speed = rand_range(ROTATION_SPEED_MIN, ROTATION_SPEED_MAX)

func _physics_process(delta):
	position += velocity * delta
	rotation_degrees += rotation_speed * delta
	if rotation_degrees > 360.0: rotation_degrees -= 360.0
	
	if position.y >= END_HEIGHT:
		if bad:
			if last: emit_signal("last")
		else:
			if not get_parent().player.dead: get_parent().player.die()
		queue_free()

func _on_Pie_body_entered(body):
	if body.is_in_group("player"):
		if bad:
			if not body.dead: body.die()
		else:
			get_node("../Pickup").play()
			if last: emit_signal("last")
		queue_free()
