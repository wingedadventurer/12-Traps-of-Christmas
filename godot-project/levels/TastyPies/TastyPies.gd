extends "res://level/Level.gd"

var scene_pie = preload("res://levels/TastyPies/Pie.tscn")
var sprite_pie_bad = preload("res://levels/TastyPies/pie_bad.png")

var NUMBER_OF_PIES = 9
var PIE_SPAWN_DELAY = 0.75

var remaining_pies
var last_spawn_index

func init():
	# player
	create_player()
	player.position = $PlayerSpawn.position
	
	remaining_pies = NUMBER_OF_PIES
	last_spawn_index = randi() % 3
	
	$PieSpawnDelay.start(PIE_SPAWN_DELAY)

func restart():
	.restart()
	$PieSpawnDelay.stop()

func spawn_pie():
	# spawning and positioning pie
	var pie = scene_pie.instance()
	add_child(pie)
	var r = randi() % 3
	while r == last_spawn_index: r = randi() % 3
	pie.position = $PieSpawns.get_children()[r].position
	last_spawn_index = r
	
	# randomly setting pie as bad
	if randi() % 3 == 0:
		pie.bad = true
		pie.get_node("Sprite").texture = sprite_pie_bad
	
	remaining_pies -= 1
	if remaining_pies == 0:
		pie.last = true
		pie.connect("last", self, "_on_last_pie")

func _on_PieSpawnDelay_timeout():
	if remaining_pies > 0:
		spawn_pie()
		$PieSpawnDelay.start(PIE_SPAWN_DELAY)

func _on_last_pie():
	if not player.dead:
		end()
