extends StaticBody2D

export var dying = true
var LIFETIME = 1.0

func _on_Area2D_body_entered(body):
	if not dying: return
	if not $LifeTimer.is_stopped(): return
	if body.is_in_group("player"):
		$LifeTimer.start(LIFETIME)
		$Tween.interpolate_property(self, "modulate", modulate, Color(1, 1, 1, 0), LIFETIME, Tween.TRANS_LINEAR, Tween.EASE_OUT)
		$Tween.start()

func _on_LifeTimer_timeout():
	queue_free()
