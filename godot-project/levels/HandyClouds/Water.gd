extends Area2D

signal water_at_end

export var rising = false
export var WATER_RISE_SPEED = 100.0
export var WATER_HEIGHT_MAX = 160.0

func _ready():
	$Animation.play("waves")

func _process(delta):
	if rising:
		position.y -= WATER_RISE_SPEED * delta
	
		if position.y <= WATER_HEIGHT_MAX:
			rising = false
			emit_signal("water_at_end")

func _on_Water_body_entered(body):
	if body.is_in_group("player"):
		body.die()
