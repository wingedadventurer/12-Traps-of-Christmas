extends "res://level/Level.gd"

var scene_cloud = preload("res://levels/HandyClouds/Cloud.tscn")
var scene_water = preload("res://level/Water.tscn")

var NUMBER_OF_CLOUDS = 6
var CLOUD_HEIGHT = 96

func init():
	var r = 0
	var r_last = 0
	
	create_player()
	player.position = $PlayerSpawn.position
	
	# clouds
	for i in NUMBER_OF_CLOUDS:
		var cloud = scene_cloud.instance()
		add_child(cloud)
		while r == r_last: r = randi() % 3
		r_last = r
		cloud.position = $CloudSpawns.get_children()[r].position
		cloud.position.y -= CLOUD_HEIGHT * i
		cloud.dying = false
#		if i == 0: # first cloud
#			player.position = cloud.position - Vector2(0, 64)
#			cloud.dying = false
#		if i == 6: # last cloud
#			cloud.dying = false
	
	# water
	var water = scene_water.instance()
	add_child(water)
	water.position = $WaterSpawn.position
	water.rising = true
	water.WATER_HEIGHT_MAX += 64
	water.connect("water_at_end", self, "_on_water_at_end")

func _on_water_at_end():
	if not player.dead:
		end()
