extends Node

const PATH_SETTINGS = "user://settings.txt"
const PATH_PROGRESS = "user://progress.txt"

var settings = {
	"version": "a2",
	
	"music": true,
	"effects": true,
	"glow": true,
	"fullscreen": false,
	"snow": true
}

var progress = {
	"version": "a2",
	
	"l1": false,
	"l2": false,
	"l3": false,
	"l4": false,
	"l5": false,
	"l6": false,
	"l7": false,
	"l8": false,
	"l9": false,
	"l10": false,
	"l11": false,
	"l12": false
}

func _ready():
	load_settings()
	load_progress()

func load_settings():
	var file = File.new()
	if not file.file_exists(PATH_SETTINGS):
		print("WARNING: cannot load, no settings.txt! Creating new...")
		save_settings()
		return
	file.open(PATH_SETTINGS, File.READ)
	settings = parse_json(file.get_line())
	file.close()

func load_progress():
	var file = File.new()
	if not file.file_exists(PATH_PROGRESS):
		print("WARNING: cannot load, no progress.txt! Creating new...")
		save_progress()
		return
	file.open(PATH_PROGRESS, File.READ)
	progress = parse_json(file.get_line())
	file.close()

func save_settings():
	var file = File.new()
	file.open(PATH_SETTINGS, File.WRITE)
	file.store_line(to_json(settings))
	file.close()

func save_progress():
	var file = File.new()
	file.open(PATH_PROGRESS, File.WRITE)
	file.store_line(to_json(progress))
	file.close()

func load_all():
	load_settings()
	load_progress()

func save_all():
	save_settings()
	save_progress()

func apply_settings():
	OS.window_fullscreen = Data.settings["fullscreen"]
	AudioServer.set_bus_mute(1, !Data.settings["music"])
	AudioServer.set_bus_mute(2, !Data.settings["effects"])
	
	var main = get_node("/root/Main")
	if main.has_node("WorldEnvironment"):
		main.get_node("WorldEnvironment").environment.glow_enabled = Data.settings["glow"]
	if main.has_node("Camera"):
		main.get_node("Camera/Snow").visible = Data.settings["snow"]

func toggle_setting(name):
	settings[name] = !settings[name]

func complete_level(level_index):
	progress["l" + str(level_index)] = true
