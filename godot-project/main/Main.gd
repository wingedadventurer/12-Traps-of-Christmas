extends Node

var path_menu = "res://menu/Menu.tscn"
var path_death_scene = "res://level/DeathScreen.tscn"
var scene_notification = "res://level/Notification.tscn"

var levels = [
	"res://levels/GiantGift/GiantGift.tscn", #1
	"res://levels/DeadlyPotions/DeadlyPotions.tscn", #2
	"res://levels/LongNeedles/LongNeedles.tscn", #3
	"res://levels/KillerBunnies/KillerBunnies.tscn", #4
	"res://levels/GoldenRings/GoldenRings.tscn", #5
	"res://levels/HandyClouds/HandyClouds.tscn", #6
	"res://levels/Turtlebacks/Turtlebacks.tscn", #7
	"res://levels/Invaders/Invaders.tscn", #8
	"res://levels/TastyPies/TastyPies.tscn", #9
	"res://levels/PoisonSpikes/PoisonSpikes.tscn", #10
	"res://levels/WalkingShrooms/WalkingShrooms.tscn", #11
	"res://levels/GodotParts/GodotParts.tscn" #12
]

var current_level_number = -1
var started_level = -1

func _ready():
	randomize()
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	load_menu()
	
	OS.window_fullscreen = Data.settings["fullscreen"]
	$WorldEnvironment.environment.glow_enabled = Data.settings["glow"]
	$Camera/Snow.visible = Data.settings["snow"]
	AudioServer.set_bus_mute(1, !Data.settings["music"])
	AudioServer.set_bus_mute(2, !Data.settings["effects"])

func _process(delta):
	if Input.is_action_just_pressed("exit"):
		if has_node("Menu"):
			get_tree().quit()
		else:
			clear_screen()
			load_menu()

func _on_level_select(level_number):
	started_level = level_number
	load_level(level_number)
	current_level_number = level_number

func load_level(level_number = -1):
	clear_screen()
	
	PlayerStats.get_node("Lives").visible = true
	if PlayerStats.lives == 0:
		PlayerStats.set_lives(PlayerStats.MAX_LIVES)
	
	if level_number == -1:
		level_number = current_level_number
	
	var level = load(levels[level_number - 1])
	if level == null: return
	level = level.instance()
	level.name = "Level"
	add_child(level)
	if level_number == 12:
		$BossMusic.play()
	else:
		if not $MainMusic.playing: $MainMusic.play()
	
	AudioServer.set_bus_volume_db(1, 0)
	$Camera/Snow.z_index = 1000

func load_previous_level():
	if current_level_number > 1:
		current_level_number -= 1
		load_level(current_level_number)
	else:
		
		Data.complete_level(started_level)
		Data.save_progress()
		load_menu()

func replay_level():
	current_level_number = started_level
	load_level(current_level_number)

func load_menu():
	clear_screen()
	PlayerStats.set_lives(0)

	var menu = load(path_menu)
	if menu == null: return
	menu = menu.instance()
	add_child(menu)
	menu.connect("level_select", self, "_on_level_select")
	$MainMusic.stop()
	$BossMusic.stop()
	AudioServer.set_bus_volume_db(1, 0)
	$Camera/Snow.z_index = -20

func load_death_screen():
	get_level().queue_free()
	var scene_death_screen = load(path_death_scene)
	var death_screen = scene_death_screen.instance()
	add_child(death_screen)
	AudioServer.set_bus_volume_db(1, -10)
	$BossMusic.stop()

func get_level():
	for child in get_children():
		if child.is_in_group("level"):
			return child
	return null

func clear_screen():
	for child in get_children():
		if child.is_in_group("level"): child.queue_free()
	
	if has_node("Menu"): get_node("Menu").queue_free()
	if has_node("DeathScreen"): get_node("DeathScreen").queue_free()
	
	$Camera.jump_to(Vector2(0, 0))
	PlayerStats.get_node("Lives").visible = false
