extends Node2D

var SHOOT_TIME = 1.0
var COLLISION_TIME = 0.5
var RECHARGE_TIME = 0.5
var HORIZONTAL_SPEED = 600.0
var MIN_XPOS = 64.0
var MAX_XPOS = 960.0

var can_shoot = false
var shooting = false
export var active = false
var velocity = Vector2()

func _ready():
	$Tween.interpolate_property(self, "modulate:a", 0, 1, 1.0, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	$Tween.start()
	
	if not active:
		position.x += 16 * (randi() % 52)
		$Beam/Sprite.frame = 0
	else:
		z_index = 8 # draw it over Godot
		$RechargeTimer.start(RECHARGE_TIME)
	velocity.x = HORIZONTAL_SPEED
	$Beam.visible = false

func _physics_process(delta):
	if active and $ShootTimer.is_stopped():
		position += velocity * delta
		if position.x < MIN_XPOS or position.x > MAX_XPOS:
			velocity.x *= -1

func shoot():
	if can_shoot:
		can_shoot = false
		shooting = true
		$ShootTimer.start(SHOOT_TIME)
		$CollisionTimer.start(COLLISION_TIME)
		$AnimationPlayer.play("shoot")
		$Shoot.play()
		get_node("/root/Main/Camera").shake(1.0, 20.0)
		get_node("../Sky").flash()

func _on_ShootTimer_timeout():
	$RechargeTimer.start(RECHARGE_TIME)
	shooting = false

func _on_RechargeTimer_timeout():
	$AnimationPlayer.play("charged")
	can_shoot = true
	$Charged.play()

func _on_CollisionTimer_timeout():
	shooting = false
