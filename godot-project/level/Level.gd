extends Node2D
class_name Level, "res://class-icons/level_icon.png"

var INIT_START_DELAY = 1.5
var START_DELAY = 0.5
var END_DELAY = 1.0

var scene_player = preload("res://player/Player.tscn")
var player = null

var scene_level_name = preload("res://level/LevelName.tscn")

export var number = 0
export var title = "x"

func _ready():
	randomize()
	$StartDelay.start(INIT_START_DELAY)
	
	var ln = scene_level_name.instance()
	add_child(ln)
	ln.get_node("Number").text = str(number)
	ln.get_node("Title").text = title

func init():
	pass

func restart():
	dispose()
	$StartDelay.start(START_DELAY)

func create_player(var position = Vector2(0, 0)):
	player = scene_player.instance()
	player.position = position
	player.connect("dead", self, "_on_Player_dead")
	add_child(player)

func dispose():
	for disposable in get_tree().get_nodes_in_group("disposable"):
		disposable.queue_free()

func end():
	$EndDelay.start(END_DELAY)
	$Overlay.fade_out(END_DELAY)
	if player != null:
		player.godmode = true

func _on_StartDelay_timeout():
	init()

func _on_EndDelay_timeout():
	get_node("/root/Main").load_previous_level()

func _on_Player_dead():
	$DeathScreen.show()
