extends CanvasLayer

var l

func _ready():
	l = get_node("Black")
	l.visible = true
	fade_in()

func fade_in(time = 0.5):
	$Tween.interpolate_property(l, "modulate:a", 1, 0, time, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	$Tween.start()

func fade_out(time = 0.5):
	$Tween.interpolate_property(l, "modulate:a", 0, 1, time, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	$Tween.start()
