extends Sprite

var default_modulate

func _ready():
	default_modulate = modulate

func flash():
	var time = 1.5
	$Tween.interpolate_property(self, "modulate", Color(0.6, 0.6, 0.0), modulate, time, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	$Tween.start()
