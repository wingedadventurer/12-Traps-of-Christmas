extends Node2D

var Tips = {
	"1":   ["Did you know that big things cast big shadows?",
			"Come on, it's the first level!\nDon't embarras me!",
			"I believe in you!"
		],
	"2":   ["Everything changes colors when under effects of poison.\nThat's why we label stuff.",
			"Don't go left!\n\nOr was it right?",
			"Pro tip:\nyou can't reach the potion without jumping at least once."
		],
	"3":   ["Come on, winning this is like a needle in a haystack!\nWait...",
			"Do you needlettle help?",
			"Patience is the KEY.\n(But what is the DOOR?)"
		],
	"4":   ["Don't feel guilty about killing them.\nThey for sure don't.",
			"You got killed by a little fluffy bunny!\nAre you proud?",
			"Go wild!",
			"You can call these lagomorphs."
		],
	"5":   ["Did you ever listen to \"12 Days of Christmas\"?",
			"Cause if you liked it, then you should have put a ring on it!!\nO O o\no o O\nO O o",
			"Take these to Mordor."
		],
	"6":   ["This is what happens when polar caps melt.",
			"You can DASH before second jump. Good to know.",
			"What's up there?",
			"Do you get to the Cloud District very often?"
		],
	"7":   ["Have you ever met Carl-bot?",
			"You can DASH anytime when in air.\nBut only once.",
			"These aren't ninja turtles.",
			"No."
		],
	"8":   ["They abducted me once.",
			"If you SLIDE, you can get there faster!",
			"See that purple line? Don't cross it."
		],
	"9":   ["Moldy pies don't taste THAT bad.",
			"You can jump out of SLIDE any time.",
			"SLIDE is faster and lasts longer than DASH.\nBut you can only SLIDE on ground."
		],
	"10":   ["We don't have any more antidote for you.",
			"I used to play this game too.\nBut then I took an arrow to the knee.",
			"Yikes, spikes!"
		],
	"11":   ["Shrooms are bouncy! Try STOMPing them!",
			"These are NOT edible.",
			"STOP EATING THEM, THEY ARE NOT EDIBLE!"
		],
	"12":   ["Legend says that one can dash through a rocket,\ndestroy it, and escape the effects of explosion!",
			"You can only damage parts that are being used.",
			"Want to buy another life for $0.99?\n...\nThis ain't that kind of game.",
			"You have learned all you can.\nDOUBLE-JUMP, DASH, SLIDE and STOMP on your way to victory!",
			"Is it pronounced GOD-oh or Go-dot?",
			"3 lives not enough?\nYou're not getting more."
		]
}

func focus():
	$ButtonYes.ignore_focus_sound = true
	$ButtonYes.grab_focus()

func unfocus():
	$ButtonNo.release_focus()
	$ButtonYes.release_focus()

func show_tip():
	var n = get_node("/root/Main").current_level_number
	var tips = Tips[str(n)]
	var tip = tips[randi() % tips.size()]
	$Tip.text = tip

#func _ready():
#	focus()
#	show_tip()

func show():
	focus()
	show_tip()
	$Animation.play("show")
	visible = true

func hide():
	unfocus()
	$Animation.play("hide")

func _on_ButtonYes_pressed():
	if PlayerStats.lives == 0:
		get_node("/root/Main").replay_level()
	else:
		get_parent().restart()
	hide()

func _on_ButtonNo_pressed():
	get_node("/root/Main").load_menu()
	queue_free()
