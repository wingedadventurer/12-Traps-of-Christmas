extends Node2D

var gift_type = "Red" setget set_gift_type

func set_gift_type(value):
	gift_type = value
	
	if not has_node("Gift"): return
	
	if gift_type == "Red": $Gift.frame = 0
	elif gift_type == "Green": $Gift.frame = 1
	elif gift_type == "Blue": $Gift.frame = 2
	elif gift_type == "Gold": $Gift.frame = 3

func _ready():
	$Animation.play("show")

func _on_Animation_animation_finished(anim_name):
	queue_free()
