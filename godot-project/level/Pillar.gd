extends KinematicBody2D

export var RISE_HEIGHT = 128.0
export var RISE_TIME = 2.0

func _ready():
	$Area/Collision.disabled = true

func rise():
	$Tween.interpolate_property(self, "position", position, position + Vector2(0, -RISE_HEIGHT), RISE_TIME, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	$Tween.start()
	$Area/Collision.disabled = false

func _on_Area_body_entered(body):
	if body.is_in_group("player") and not body.dead:
		get_parent().end()
		$Area/Collision.call_deferred("set_disabled", true)
