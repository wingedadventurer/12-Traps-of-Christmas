extends Node2D

var texture_heart_full = preload("res://level/heart_full.png")
var texture_heart_empty = preload("res://level/heart_empty.png")

var MAX_LIVES = 3

var lives = 0 setget set_lives

func _ready():
	# setup lives
	for i in range(MAX_LIVES):
		var s = Sprite.new()
		s.name = "Life" + str(i + 1)
		s.texture = texture_heart_empty
		s.scale *= 4
		s.position += Vector2(40 * i, 0)
		$Lives.add_child(s)
	set_lives(lives)

func set_lives(value):
	lives = value
	if lives < 0: lives = 0
	
	for i in $Lives.get_child_count():
		if lives >= i + 1:
			$Lives.get_child(i).texture = texture_heart_full
		else:
			$Lives.get_child(i).texture = texture_heart_empty

func reduce_lives(amount = -1):
	set_lives(lives + amount)
