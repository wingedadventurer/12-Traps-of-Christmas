extends KinematicBody2D

signal dead
signal ground_hit

var sprite_move = preload("res://player/player_move.png")
var sprite_dash = preload("res://player/player_dash.png")
var sprite_slide = preload("res://player/player_slide.png")
var sprite_shoot = preload("res://player/player_shoot.png")
var sprite_stomp = preload("res://player/player_stomp.png")
var scene_bullet = preload("res://player/Bullet.tscn")
var scene_death = preload("res://player/PlayerDeath.tscn")

var godmode = false
var damage = false

enum MoveState {
	MOVING,
	SLIDING,
	DASHING,
	STOMPING
}

var HORIZONTAL_VELOCITY = 400.0
var JUMP_STRENGTH = 650.0
var DOUBLEJUMP_STRENGTH = 800.0
var GRAVITY_STRENGTH = 45.0
var SLIDE_TIME = 0.4
var SLIDE_VELOCITY_FACTOR = 2.2
var SLIDE_VELOCITY = 1000.0
var DASH_TIME = 0.2
var DASH_VELOCITY = 1200.0
var DASH_VELOCITY_UP = 600.0
var STOMP_VELOCITY = 2400.0

var STOMP_TIME = 2.0
var SHOOT_DELAY = 0.4
var DEATH_TIME = 1.0
var SHOOT_TIME = 0.1
var ON_GROUND_EXTENDED_TIME = 0.1

var BOUNCE_NORMAL = 850.0
var BOUNCE_STOMP = 1300.0
var BOUNCE_LAST = 2200.0

var velocity = Vector2()
var double_jumped = false
var on_ground = false
var can_dash = false
var move_state = MoveState.MOVING

var dead = false

func _ready():
	$ShootDelay.wait_time = SHOOT_DELAY
	set_sprite(sprite_move)
	
	# fade in spawn effect
	$Sprite.modulate.a = 0
	$Tween.interpolate_property($Sprite, "modulate:a", 0, 1, 0.2, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	$Tween.start()

func _physics_process(delta):
	if dead: return
	
	if move_state == MoveState.MOVING:
		
		# check if on ground
		check_ground()
		if on_ground:
			can_dash = true
			double_jumped = false
			$AnimationPlayer.playback_active = true
		else:
			$AnimationPlayer.playback_active = false
		
		# change state to shoot
		if Input.is_action_pressed("shoot") and $ShootDelay.is_stopped():
			create_bullet()
			$ShootDelay.start()
			set_sprite(sprite_shoot)
			$StateTimer.start(SHOOT_TIME)
			$ShootDelay.start(SHOOT_DELAY)
		
		# change state to stomp
		if not on_ground:
			if Input.is_action_just_pressed("stomp"):
				stomp()
				return
		
		# change state to slide or dash
		if Input.is_action_just_pressed("dashslide"):
			if on_ground:
				if velocity.x != 0:
					move_state = MoveState.SLIDING
#					velocity.x *= SLIDE_VELOCITY_FACTOR
					velocity.x = SLIDE_VELOCITY * sign(velocity.x)
					velocity.y = 0
					set_sprite(sprite_slide)
					$Roll.play()
					$StateTimer.wait_time = SLIDE_TIME
					$StateTimer.start()
					return
			else:
				if can_dash and velocity.x != 0:
					velocity.x = DASH_VELOCITY * sign(velocity.x)
					velocity.y = 0
					move_state = MoveState.DASHING
					can_dash = false
					set_sprite(sprite_dash)
					$Dash.play()
					$StateTimer.start(DASH_TIME)
					return
		
		# horizontal movement
		velocity.x = 0
		if Input.is_action_pressed("left"):
			velocity.x -= HORIZONTAL_VELOCITY
		if Input.is_action_pressed("right"):
			velocity.x += HORIZONTAL_VELOCITY
		
		if velocity.x != 0:
			if not $AnimationPlayer.is_playing():
				$AnimationPlayer.play("walk")
		else:
			if $AnimationPlayer.is_playing():
				$AnimationPlayer.stop(true)
				$Sprite.frame = 0
	
		# applying gravity
		velocity.y += GRAVITY_STRENGTH

		# vertical movement (jump)
		if Input.is_action_just_pressed("jump"):
			if on_ground or not $OnGroundExtendedTime.is_stopped():
				$OnGroundExtendedTime.stop()
				$OnGroundIgnoreTime.start()
				velocity.y = -JUMP_STRENGTH
				double_jumped = false
				$Jump.play()
			elif not double_jumped:
				$OnGroundExtendedTime.stop()
				$OnGroundIgnoreTime.start()
				velocity.y = -JUMP_STRENGTH
				double_jumped = true
				$Jump2.play()
	
		# applying velocity
		velocity = move_and_slide(velocity, Vector2(0, -1))
		if get_slide_count() > 0:
			var collision = get_slide_collision(0)
			for group in collision.collider.get_groups():
				if group == "bounceable":
					bounce(BOUNCE_NORMAL)

	elif move_state == MoveState.SLIDING:
		if Input.is_action_just_pressed("jump"):
			if on_ground or not $OnGroundExtendedTime.is_stopped():
				$OnGroundExtendedTime.stop()
				$OnGroundIgnoreTime.start()
				velocity.y = -JUMP_STRENGTH
				double_jumped = false
				$Jump.play()
				_on_StateTimer_timeout()
		
		# applying velocity
		velocity = move_and_slide(velocity, Vector2(0, -1))
	
	elif move_state == MoveState.DASHING:
		if Input.is_action_just_pressed("stomp"):
			stomp()
			
		# applying velocity
		velocity = move_and_slide(velocity, Vector2(0, -1))
		if get_slide_count() > 0:
			var collision = get_slide_collision(0)
			for group in collision.collider.get_groups():
				if group == "bounceable":
					bounce(BOUNCE_NORMAL)
	
	elif move_state == MoveState.STOMPING:
		# check if on ground
		check_ground()
		if on_ground:
			move_state = MoveState.MOVING
			set_sprite(sprite_move)
			emit_signal("ground_hit")
		
		# applying velocity
#		var collision = move_and_collide(velocity * delta)
#		if collision != null:
#			_on_StateTimer_timeout()
		
		velocity = move_and_slide(velocity, Vector2(0, -1))
		if get_slide_count() > 0:
			var collision = get_slide_collision(0)
			for group in collision.collider.get_groups():
				if group == "bounceable":
					if collision.collider.last:
						bounce(BOUNCE_LAST)
					else:
						bounce(BOUNCE_STOMP)

func check_ground():
	if not $OnGroundIgnoreTime.is_stopped() and not move_state == MoveState.STOMPING:
		on_ground = false
		return
	for body in $GroundCheck.get_overlapping_bodies():
		if body.is_in_group("walls") and velocity.y >= 0:
			on_ground = true
			return
	if on_ground:
		$OnGroundExtendedTime.start(ON_GROUND_EXTENDED_TIME)
	on_ground = false

func stomp():
	velocity.x = 0
	velocity.y = STOMP_VELOCITY
	move_state = MoveState.STOMPING
	$StateTimer.start(STOMP_TIME)
	$Stomp.play()
	set_sprite(sprite_stomp)

func set_sprite(sprite):
	if sprite == sprite_move:
		$Sprite.hframes = 6
		$Sprite.frame = 0
	else:
		$Sprite.hframes = 1
		$Sprite.frame = 0
	
	$Sprite.texture = sprite
	if sprite == sprite_move:
		$AnimationPlayer.play("walk")
	else:
		$AnimationPlayer.stop(true)
	
	if sign(velocity.x) == 1:
		$Sprite.scale.x = 4
	if sign(velocity.x) == -1:
		$Sprite.scale.x = -4

func create_bullet():
	var bullet = scene_bullet.instance()
	bullet.position = position
	get_parent().add_child(bullet)
	$Shoot.play()

func die():
	if godmode: return
	if $DamageAnim.is_playing(): return
	$Death.play()
	
	PlayerStats.reduce_lives()
	
	if damage and PlayerStats.lives > 0:
		$DamageAnim.play("damage")
	else:
		# creating death effect
		var death = scene_death.instance()
		death.position = position
		death.DEATH_TIME = DEATH_TIME
		death.modulate = modulate # for poisoned effect
		get_parent().add_child(death)
		death.z_index = z_index
		
		# disabling and hiding
		dead = true
		$Collision.call_deferred("set_disabled", true)
		visible = false
		$DeathTimer.start(DEATH_TIME)

func bounce(amount):
	move_state = MoveState.MOVING
	set_sprite(sprite_move)
	velocity.x = 0
	velocity.y = -amount
	can_dash = true
	double_jumped = true
	$BigJump.play()

func _on_StateTimer_timeout():
	move_state = MoveState.MOVING
	set_sprite(sprite_move)

func _on_DeathTimer_timeout():
	emit_signal("dead")
