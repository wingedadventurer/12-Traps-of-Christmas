extends Area2D

var SPEED = 1200.0
var velocity = Vector2()

func _ready():
	velocity.y = -SPEED

func _physics_process(delta):
	position += velocity * delta
