extends Particles2D

var DEATH_TIME = 1.5

func _ready():
	$Timer.start(DEATH_TIME)

func _on_Timer_timeout():
	queue_free()
