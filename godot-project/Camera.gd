extends Camera2D

var DEFAULT_REPOSITION_TIME = 0.8
var DEFAULT_SHAKE_TIME = 1.0
var DEFAULT_SHAKE_STRENGTH = 10.0

var shake_time = 0.0
var shake_strength = 1.0

func _process(delta):
	if shake_time > 0:
		offset = Vector2(1, 1).rotated(deg2rad(rand_range(0, 360.0))) * shake_time * shake_strength
		shake_time -= delta

func reposition(target_position):
	$Tween.interpolate_property(self, "position", position, target_position, DEFAULT_REPOSITION_TIME, Tween.TRANS_CUBIC, Tween.EASE_OUT)
	$Tween.start()

func jump_to(new_position):
	$Tween.stop_all()
	position = new_position

func shake(time = DEFAULT_SHAKE_TIME, strength = DEFAULT_SHAKE_STRENGTH):
	shake_time = time
	shake_strength = strength
